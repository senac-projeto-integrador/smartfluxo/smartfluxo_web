import React from 'react'
import axios from 'axios'
import './Altera_cliente.css'


const initialState = {
    user: { nome: '', email: '', endereco: '', numero: '', bairro: '', cep: '', cidade: '' }
}

export default class UserCrud extends React.Component {

    state = { ...initialState }

    clear(event) {
        event.preventDefault()
        this.setState({ user: initialState.user })
    }

    save(event) {
        event.preventDefault()
        const user = this.state.user
        const id = this.props.match.params.id
        console.log(id);
        axios.put(`http://localhost:3000/cliente/${Number(id)}`, user)
            .then(resp => {
                this.props.history.push("/gerenciamento_clientes")
                this.setState({ user: initialState.user })
                console.log(resp.data)

            })
    }

    updateField(event) {

        const user = { ...this.state.user }
        user[event.target.name] = event.target.value
        this.setState({ user })
    }

    render() {
        return (

            <div>
                <div className="menu1">
                    <div >
                        <header className="navbar1"><center><h1>Smartfluxo</h1></center> </header>
                    </div>
                    
                </div>
                <div className="formulario1">

                    <h2>Atualização de dados do Cliente</h2>
                    <form >
                        <div  >
                            <label  >Nome:</label>
                            <input className='input1' type="text" name="nome" value={this.state.user.nome} onChange={e => this.updateField(e)} />
                        </div>
                        <div>
                            <label >e-mail:</label>
                            <input className='input1' type="email" name="email" value={this.state.user.email} onChange={e => this.updateField(e)} />
                        </div>
                        <div>
                            <label >endereco:</label>
                            <input className='input1' type="text" name="endereco" value={this.state.user.endereco} onChange={e => this.updateField(e)} required />
                        </div>
                        <div>
                            <label >numero:</label>
                            <input className='input1' type="text" name="numero" value={this.state.user.numero} onChange={e => this.updateField(e)} required />
                        </div>
                        <div>
                            <label >bairro:</label>
                            <input className='input1' type="text" name="bairro" value={this.state.user.bairro} onChange={e => this.updateField(e)} required/>
                        </div>
                        <div>
                            <label >cep:</label>
                            <input className='input1' type="text" name="cep" value={this.state.user.cep} onChange={e => this.updateField(e)} required />
                        </div>
                        <div>
                            <label >cidade:</label>
                            <input className='input1' type="text" name="cidade" value={this.state.user.cidade} onChange={e => this.updateField(e)} required />
                        </div>
                      
                        <div>
                            <button className="register" onClick={e => this.save(e)}>Atualizar</button>

                            <button className="cancel" onClick={e => this.clear(e)}>Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        )


    }
}



