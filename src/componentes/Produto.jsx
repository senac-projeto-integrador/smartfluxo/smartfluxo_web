import React from 'react'
import axios from 'axios'
import './Produto.css'
import Menu from './Menu'

const initialState = {
    produto: { descricao: '', tipo: '', quantidade_estoque: '', valor_compra: '', valor_venda: '', foto: '' }
}

export default class UserCrud extends React.Component {

    state = { ...initialState }

    clear(event) {
        event.preventDefault()
        this.setState({ produto: initialState.produto })
    }

    save(event) {
        event.preventDefault()
        const produto = this.state.produto
        axios.post('http://localhost:3000/Produto/', produto)
            .then(resp => {
                this.setState({ produto: initialState.produto })
                console.log(resp.data)

            })
    }

    updateField(event) {

        const produto = { ...this.state.produto }
        produto[event.target.name] = event.target.value
        this.setState({ produto })
    }

    render() {
        return (
            <div>
                <Menu />
                <div className="formulario">
                    <h2>Cadastro de Produtos</h2>
                    <form >
                        <div>
                            <label  >Tipo:</label>
                            <input className='input' type="text" name="descricao" value={this.state.produto.descricao} onChange={e => this.updateField(e)} />
                        </div>
                        <div>
                            <label >Descrição:</label>
                            <input className='input' type="email" name="tipo" value={this.state.produto.tipo} onChange={e => this.updateField(e)} />
                        </div>
                        <div>
                            <label >Unidades:</label>
                            <input className='input' type="text" name="quantidade_estoque" value={this.state.produto.quantidade_estoque} onChange={e => this.updateField(e)} required />
                        </div>
                        <div>
                            <label >Valor De Compra:</label>
                            <input className='input' type="text" name="valor_compra" value={this.state.produto.valor_compra} onChange={e => this.updateField(e)} required />
                        </div>
                        <div>
                            <label >Valor De Venda:</label>
                            <input className='input' type="text" name="valor_venda" value={this.state.produto.valor_venda} onChange={e => this.updateField(e)} required />
                        </div>
                        <div>
                            <label >URL da Foto do Produto</label>
                            <input className='input' type="text" name="foto" value={this.state.produto.foto} onChange={e => this.updateField(e)} required />
                        </div>

                        <div>
                            <button className="register" onClick={e => this.save(e)}>Salvar</button>

                            <button className="cancel" onClick={e => this.clear(e)}>Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        )


    }
}