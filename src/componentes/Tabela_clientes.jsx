import React from 'react';
import "./Tabela_cliente.css"
import axios from 'axios';
import Menu from './Menu'
import { BiTrash } from 'react-icons/bi'
import { GrDocumentUpdate } from 'react-icons/gr'


export default class PersonList extends React.Component {
    state = {
        persons: []
    }

    componentDidMount() {
        axios.get(`http://localhost:3000/cliente/`)
            .then(res => {
                const persons = res.data;
                console.log(persons);
                this.setState({ persons });
            })
    }

    handleRemoverlinha = async (e) => {
        
        const response = await axios.delete(`http://localhost:3000/cliente/${Number(e)}`)
        if (response.status === 204) {
            alert(`Cliente Excluido`)            
        }
        this.setState(this.componentDidMount)


    }

    handleatualiza = async (e) => {        
        console.log(e)
        this.props.history.push("/alteracliente/" + Number(e))

    }

    render() {
        return (
            <div>
                <Menu />
                <table className='tabela'>
                    <thead>
                        <tr>
                            
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Endereco</th>
                            <th>Numero</th>
                            <th>Bairro</th>
                            <th>Cep</th>
                            <th>Cidade</th>
                            <th>Alterar/Excluir</th>
                        </tr>
                    </thead>
                    <tbody>

                        {this.state.persons.map(person =>
                            <tr>
                                
                                <td>{person.nome}</td>
                                <td>{person.email}</td>
                                <td>{person.endereco}</td>
                                <td>{person.numero}</td>
                                <td>{person.bairro}</td>
                                <td>{person.cep}</td>
                                <td>{person.cidade}</td>
                                <td><GrDocumentUpdate onClick={(e) => this.handleatualiza(person.id, e)} />< BiTrash onClick={(e) => this.handleRemoverlinha(person.id, e)} /></td>
                            </tr>
                        )}

                    </tbody>
                </table>
            </div>
        )
    }
}